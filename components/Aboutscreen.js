import React, { Component } from "react";
  
  import { Button, View, Text,StyleSheet,TextInput,TouchableOpacity,Image } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import PhoneInput from "react-native-phone-number-input";

export default class Aboutscreen extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.Text}>RENTON</Text>
        <TouchableOpacity 
        style={styles.TextInputStyleClass}
        onPress={() => this.props.navigation.navigate("Phone")}
        >
          <Text style={styles.Numbertext}>Mobile Number</Text> 
        </TouchableOpacity>
        {/* <TextInput
        onPress={() => this.props.navigation.navigate("Phone")}
          placeholder="Mobile Number"
          underlineColorAndroid='transparent'
          style={styles.TextInputStyleClass}/> */}
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Phone")}
          style={styles.button}
        >
          <Text style={styles.buttonText}>Facebook</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button1}
        >
          <Text style={styles.buttonText}>Google</Text>
        </TouchableOpacity>
        
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container:{
    borderRadius: 12,
    flex: 1,
    backgroundColor:"#918B81",

  },
  TextInputStyleClass:{
    textAlign: 'center',
    height: 50,
    borderWidth: 2,
    borderColor: '#FF5722',
    borderRadius: 20 , 
    backgroundColor : "#FFFFFF",
    top:150,
    margin:60,
    
    
    },
    Text:{
      fontWeight:"bold",
      fontSize:40,
      alignContent:"center",
      margin:10,
      textAlign:"center",
      top:200,
      
      
    },
    button: {
      borderRadius: 20,
      paddingVertical: 12,
      paddingHorizontal: 12,
      backgroundColor: "#FA5C5C",
      marginTop: 350,
      width: 100,
      justifyContent: "center",
      alignItems: "center",
      margin: 85,
      position: "absolute",
      height:10,
      
      
    },
    button1: {
      borderColor: '#FF5722',
      backgroundColor : "#FFFFFF",
      borderRadius: 20,
      paddingVertical: 12,
      paddingHorizontal: 12,
      backgroundColor: "#FFFFFF",
      marginTop: 350,
      width: 100,
      justifyContent: "center",
      alignItems: "center",
      margin: 200,
      position: "absolute",
      height:10,   
    },
    Numbertext:{
      alignContent:"center",
      margin:10,
      alignItems:"center",
      textAlign:"center",
      fontSize:17
    }
   
})
