import 'react-native-gesture-handler';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './components/Homescreen';
import FriendScreen from './components/Aboutscreen'
import PhoneScreen from './src/screen/phoneDetails'
import cartDetails from './src/screen/cartDeatails'
import DetailsScreen from './src/screen/detailPage'

const Stack = createStackNavigator();
export default function App() {
  return (
    <NavigationContainer>
        <Stack.Navigator>
        <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{ headerShown: false }}
            
          />
           <Stack.Screen
              name="Friends"
              component={FriendScreen}
              options={{ headerShown: false }}
              
              
            />
            <Stack.Screen
              name="Phone"
              component={PhoneScreen}
              options={{ headerShown: false }}
            />
            <Stack.Screen
              name="Cart"
              component={cartDetails}
              // options={{ headerShown: false }}
            />
             <Stack.Screen
              name="Details"
              component={DetailsScreen}
            />
        </Stack.Navigator>
      </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
