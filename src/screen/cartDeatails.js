import React, { Component } from "react";
import {  Button, View, Text,StyleSheet,Dimensions,TextInput,TouchableOpacity,Image,ScrollView } from "react-native";
 import { Card } from 'react-native-elements';
 import ImagesSwiper from "react-native-image-swiper";
 import CardScreen from '../screen/cardScreen'
//  import phoneDetails from './phoneDetails'
//  import ActionBar from 'react-native-action-bar'
 import { useNavigation } from '@react-navigation/native'

//  import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage } from 'react-native-cards';
 const { width, height } = Dimensions.get("window");
 const customImg = [
   "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQv84cF07ATzKfaSPV-NQeTgvZEds0PuUe3MA&usqp=CAU",
   "https://i.ytimg.com/vi/ZYQsR8hSeMU/maxresdefault.jpg",
   "https://images-na.ssl-images-amazon.com/images/I/71buomNVeFL._SL1500_.jpg",
   "https://img2.exportersindia.com/product_images/bc-full/dir_97/2889526/tree-cutter-1731699.jpg",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS-aZGmyGP1NdiMt1Dyhvr82TyYTwFdd0oUNQ&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRkbPjhbId7gqW6ypxqQrUJ6HAksV32eWxRdw&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQIsIgNSRsfWHCaEk2edAtjLmaJIOukBl2fPQ&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT6KkS4JbUOImEmUJTejW7b8p3fUKy3Ee_j0A&usqp=CAU",
    "https://previews.123rf.com/images/argus456/argus4561605/argus456160500568/55953606-renton-red-grunge-rubber-stamp-on-white-background.jpg",
    "https://previews.123rf.com/images/argus456/argus4561605/argus456160500568/55953606-renton-red-grunge-rubber-stamp-on-white-background.jpg"
  ];
  
  const cartDetails = () => {
    
      const navigation = useNavigation()
      const detalhes = () => navigation.navigate('Details')
   
    
    
      return (
        
        <View style={styles.container}>
          {/* <ActionBar
            containerStyle={{height:60,alignSelf: 'center',paddingRight:40}}
            backgroundColor={'#fff'}
            title={'Gallery'}
            titleStyle={styles.pageTitle}
            onLeftPress={() => goBack()}
            leftIconContainerStyle={{marginTop:22}}
            leftIconName={'back'}
            leftIconImageStyle={{tintColor: '#000000'}}
          /> */}
          <ScrollView style={{flex: 1}}>
            <ImagesSwiper style={styles.Swiper} 
              images={customImg}
              autoplay={true} 
              autoplayTimeout={1.5}
              showsPagination={false}
              width={width} 
              height={180}
            />
            
          <ScrollView>
           <CardScreen title="GRASS CUTTER" imagetoShow={require('../../assets/grass.jpg')}></CardScreen>
           <CardScreen title="DRILLING MACHINE" imagetoShow={require('../../assets/dril.jpg')}></CardScreen>
           <CardScreen title="TREE CUTTER" imagetoShow={require('../../assets/cutter.jpg')}></CardScreen>
           <CardScreen title="BIKE" imagetoShow={require('../../assets/bike1.jpg')}></CardScreen>
           <CardScreen title="CHAIN PULLEY" imagetoShow={require('../../assets/pulley.jpg')}></CardScreen>
           </ScrollView>
           </ScrollView>
        </View>
      );
    }
  
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 30,
      backgroundColor:"#554A3A"
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      marginTop: 30,
      marginBottom: 10,
    },
    cardview:{
      height:10,
      width:1,
      alignItems:"center",  
    },
    Image1:{
      height:20,
      width:20
    },
    Swiper:{
      borderRadius:10
    }
  });
export default cartDetails;
